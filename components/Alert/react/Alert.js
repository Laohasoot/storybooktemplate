
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';

const Alert = ({
  className, children, intent, dismissible, fade, show, style,
}) => {
  let mutableClassName = 'alert';
  if (intent) mutableClassName = `${mutableClassName} alert-${intent}`;
  if (dismissible) mutableClassName = `${mutableClassName} alert-dismissible`;
  if (fade) mutableClassName = `${mutableClassName} fade`;
  if (show) mutableClassName = `${mutableClassName} show`;

  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} role="alert" style={style}>{children}</div>;
};

Alert.defaultProps = {
  style: null,
  className: '',
  children: null,
  intent: 'primary',
  dismissible: false,
  fade: false,
  show: false,
};

Alert.propTypes = {
  style: stylePropType,
  className: PropTypes.string,
  intent: PropTypes.string,
  dismissible: PropTypes.bool,
  fade: PropTypes.bool,
  show: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Alert);
