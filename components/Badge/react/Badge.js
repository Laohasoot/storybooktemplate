
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Badge = ({
  className,
  children,
  intent,
  href,
  pill,
  style,
}) => {
  let mutableClassName = 'badge';
  if (pill) mutableClassName = `${mutableClassName} badge-pill`;
  if (intent) mutableClassName = `${mutableClassName} badge-${intent}`;
  mutableClassName = `${mutableClassName} ${className}`;
  if (href) return <a href={href} className={mutableClassName} style={style} >{children}</a>;
  return <span className={mutableClassName} style={style} >{children}</span>;
};

Badge.defaultProps = {
  intent: 'secondary',
  className: '',
  style: null,
  children: null,
  href: null,
  pill: false,
};

Badge.propTypes = {
  pill: PropTypes.bool,
  href: PropTypes.string,
  intent: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Badge);
