
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';

const Box = ({
  className, children, id, name, style,
}) => {
  const mutableClassName = `${className}`;
  return <div className={mutableClassName} id={id} name={name} style={style}>{children}</div>;
};

Box.defaultProps = {
  style: null,
  id: null,
  name: null,
  className: '',
  children: null,
};

Box.propTypes = {
  style: stylePropType,
  id: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Box);
