import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Button = ({
  type, intent, className, children, style, ...props
}) => {
  let mutableClassName = 'btn';
  if (intent) mutableClassName = `${mutableClassName} btn-${intent}`;
  mutableClassName = `${mutableClassName} ${className}`;
  return (<button type="button" className={mutableClassName} style={style} {...props} >{children}</button>);
};

Button.defaultProps = {
  intent: 'primary',
  type: 'button',
  children: null,
  className: '',
  style: null,
};

Button.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  intent: PropTypes.string,
  type: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),

};

export default UtilComponent(Button);
