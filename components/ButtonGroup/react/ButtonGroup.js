
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const ButtonGroup = ({
  className, children, dropDirection, style,
}) => {
  let mutableClassName = `btn-group ${className}`;
  if (dropDirection) mutableClassName = `${mutableClassName} drop${dropDirection}`;
  return <div className={mutableClassName} style={style} >{children}</div>;
};

ButtonGroup.defaultProps = {
  className: '',
  style: null,
  dropDirection: null,
  children: null,
};

ButtonGroup.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  dropDirection: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ButtonGroup);
