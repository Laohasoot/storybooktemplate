
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Caption = ({
  className, children, style, id, name,
}) => {
  const mutableClassName = `${className}`;
  return (
    <caption
      className={mutableClassName}
      style={style}
      id={id}
      name={name}
    >{children}
    </caption>
  );
};

Caption.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

Caption.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Caption);
