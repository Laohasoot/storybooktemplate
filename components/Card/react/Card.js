
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Card = ({ className, children, style }) => {
  const mutableClassName = `card ${className}`;
  return (
    <div className={mutableClassName} style={style} >{children}</div>
  );
};


Card.defaultProps = {
  className: '',
  style: null,
  children: null,
};

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
  style: stylePropType,
};


export default UtilComponent(Card);
