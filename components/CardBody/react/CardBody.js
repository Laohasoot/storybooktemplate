
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardBody = ({ className, children, style }) => {
  const mutableClassName = `card-body ${className}`;
  return <div className={mutableClassName} style={style} >{children}</div>;
};

CardBody.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardBody.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
  style: stylePropType,
};


export default UtilComponent(CardBody);
