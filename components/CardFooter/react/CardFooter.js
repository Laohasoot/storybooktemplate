
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardFooter = ({ className, children, style }) => {
  const mutableClassName = `card-footer ${className}`;
  return <div className={mutableClassName} style={style} >{children}</div>;
};

CardFooter.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardFooter.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardFooter);
