
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardHeader = ({ className, children , style })=> {
  const mutableClassName = `card-header ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

CardHeader.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardHeader.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardHeader);
