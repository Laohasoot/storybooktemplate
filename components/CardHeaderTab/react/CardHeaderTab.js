
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardHeaderTab = ({ className, children, pills , style })=> {
  const mutableClassName = pills ? `nav nav-pills card-header-pills ${className}` : `nav nav-tabs card-header-tabs ${className}`;
  return <ul className={mutableClassName} style={style}  >{children}</ul>;
};

CardHeaderTab.defaultProps = {
  className: '',
  style: null,
  children: null,
  pills: false,
};

CardHeaderTab.propTypes = {
  pills: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardHeaderTab);
