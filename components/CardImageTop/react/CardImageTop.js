
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const CardImageTop = ({ className, src, alt , style })=> {
  const mutableClassName = `card-img-top ${className}`;
  return <img className={mutableClassName} style={style} src={src} alt={alt} />;
};

CardImageTop.defaultProps = {
  className: '',
  style: null,
  alt: '',
  src: '',
};

CardImageTop.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
};

export default UtilComponent(CardImageTop);
