
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardLink = ({ className, href, children , style })=> {
  const mutableClassName = `card-link ${className}`;
  return <a className={mutableClassName} style={style} href={href} >{children}</a>;
};

CardLink.defaultProps = {
  href: '#',
  className: '',
  style: null,
  children: null,
};

CardLink.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardLink);
