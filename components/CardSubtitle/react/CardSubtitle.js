
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardSubtitle = ({ className, children , style })=> {
  const mutableClassName = `card-subtitle ${className}`;
  return <h6 className={mutableClassName} style={style}  >{children}</h6>;
};

CardSubtitle.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardSubtitle.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardSubtitle);
