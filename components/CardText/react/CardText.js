
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const CardText = ({ className, children , style })=> {
  const mutableClassName = `card-text ${className}`;
  return <p className={mutableClassName} style={style}  > {children} </p>;
};

CardText.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardText.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  className: PropTypes.string,
  style: stylePropType,
};

export default UtilComponent(CardText);
