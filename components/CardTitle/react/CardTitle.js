
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const CardTitle = ({ className, children , style })=> {
  const mutableClassName = `card-title ${className}`;
  return <h5 className={mutableClassName} style={style}  >{children}</h5>;
};

CardTitle.defaultProps = {
  className: '',
  style: null,
  children: null,
};

CardTitle.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(CardTitle);
