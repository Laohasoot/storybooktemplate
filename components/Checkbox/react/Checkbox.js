
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Checkbox = ({ className, style, ...props }) => {
  const mutableClassName = `form-check-input ${className}`;
  return <input className={mutableClassName} style={style} type="checkbox" {...props} />;
};

Checkbox.defaultProps = {
  className: '',
  style: null,
  children: null,
};

Checkbox.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Checkbox);
