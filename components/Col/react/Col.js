
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';

const Col = ({
  className,
  children,
  colWidth,
  colSmallWidth,
  colMediumWidth,
  colLargeWidth,
  colExtraLargeWidth,
  style,
}) => {
  let mutableClassName = 'col';
  const col = 'col-';
  const colsm = 'col-sm-';
  const colmd = 'col-md-';
  const collg = 'col-lg-';
  const colxl = 'col-xl-';

  if (colWidth) mutableClassName = `${mutableClassName} ${col}${colWidth}`;
  if (colSmallWidth) mutableClassName = `${mutableClassName} ${colsm}${colSmallWidth}`;
  if (colMediumWidth) mutableClassName = `${mutableClassName} ${colmd}${colMediumWidth}`;
  if (colLargeWidth) mutableClassName = `${mutableClassName} ${collg}${colLargeWidth}`;
  if (colExtraLargeWidth) mutableClassName = `${mutableClassName} ${colxl}${colExtraLargeWidth}`;

  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style} >{children}</div>;
};

Col.defaultProps = {
  colWidth: null,
  colSmallWidth: null,
  colMediumWidth: null,
  colLargeWidth: null,
  colExtraLargeWidth: null,
  className: '',
  style: null,
  children: null,
};

Col.propTypes = {
  colWidth: PropTypes.number,
  colSmallWidth: PropTypes.number,
  colMediumWidth: PropTypes.number,
  colLargeWidth: PropTypes.number,
  colExtraLargeWidth: PropTypes.number,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Col);
