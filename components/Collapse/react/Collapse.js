
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Collapse = ({
  className, children, isNavbar, id,
  style,
}) => {
  let mutableClassName = 'collapse';
  if (isNavbar) mutableClassName = `${mutableClassName} navbar-collapse`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style} id={id}>{children}</div>;
};

Collapse.defaultProps = {
  id: null,
  isNavbar: false,
  className: '',
  style: null,
  children: null,
};

Collapse.propTypes = {
  id: PropTypes.string,
  isNavbar: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Collapse);
