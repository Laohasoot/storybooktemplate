import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Container = ({
  className, children, full, style,
}) => {
  let mutableClassName = full ? 'container-fluid' : 'container';
  mutableClassName = `${mutableClassName} ${className}`;
  return (
    <div className={mutableClassName} style={style} >
      {children}
    </div>
  );
};

Container.defaultProps = {
  className: '',
  style: null,
  children: null,
  full: false,
};

Container.propTypes = {
  full: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(Container);
