
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Dropdown = ({
  className, children, show, dropDirection,
  style,
}) => {
  let mutableClassName = `drop${dropDirection}`;
  if (show) mutableClassName = `${mutableClassName} show`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style} >{children}</div>;
};

Dropdown.defaultProps = {
  className: '',
  style: null,
  children: null,
  show: false,
  dropDirection: 'down',
};

Dropdown.propTypes = {
  show: PropTypes.bool,
  dropDirection: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Dropdown);
