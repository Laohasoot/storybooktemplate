
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const DropdownDivider = ({ className, children , style })=> {
  const mutableClassName = `dropdown-divider ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

DropdownDivider.defaultProps = {
  className: '',
  style: null,
  children: null,
};

DropdownDivider.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(DropdownDivider);
