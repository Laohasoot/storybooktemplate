
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const DropdownHeader = ({ className, children , style })=> {
  const mutableClassName = `dropdown-header ${className}`;
  return <h6 className={mutableClassName} style={style}  >{children}</h6>;
};

DropdownHeader.defaultProps = {
  className: '',
  style: null,
  children: null,
};

DropdownHeader.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(DropdownHeader);
