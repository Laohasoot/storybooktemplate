
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const DropdownItem = ({
  className, children, href, disabled, style 
}) => {
  let mutableClassName = 'dropdown-item';
  if (disabled) mutableClassName = `${mutableClassName} disabled`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <a className={mutableClassName} style={style} href={href}>{children}</a>;
};

DropdownItem.defaultProps = {
  disabled: false,
  className: '',
  style: null,
  children: null,
  href: '#',
};

DropdownItem.propTypes = {
  disabled: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  href: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(DropdownItem);
