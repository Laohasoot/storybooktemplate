
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const DropdownMenu = ({
  className, children, position, ariaLabelledby, style }) => {
  let mutableClassName = `dropdown-menu ${className}`;
  if (position) mutableClassName = `${mutableClassName} dropdown-menu-${position}`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style} aria-labelledby={ariaLabelledby}>{children}</div>;
};

DropdownMenu.defaultProps = {
  className: '',
  style: null,
  children: null,
  position: null,
  ariaLabelledby: '',
};

DropdownMenu.propTypes = {
  position: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  ariaLabelledby: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(DropdownMenu);
