
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const DropdownToggle = ({
  className, children, href, intent, split, ariaHaspopup, ariaExpanded, id, name, style }) => {
  let mutableClassName = 'btn';
  if (intent) mutableClassName = `${mutableClassName} btn-${intent}`;
  mutableClassName = `${mutableClassName} dropdown-toggle`;
  if (split) mutableClassName = `${mutableClassName} dropdown-toggle-split`;
  mutableClassName = `${mutableClassName} ${className}`;
  if (href) {
    return (
      <a
        className={mutableClassName} style={style}
        href={href}
        id={id}
        name={name}
        data-toggle="dropdown"
        aria-haspopup={ariaHaspopup.toString()}
        aria-expanded={ariaExpanded.toString()}
      >children
      </a>
    );
  }
  return (
    <button
      type="button"
      className={mutableClassName} style={style}
      id={id}
      name={name}
      data-toggle="dropdown"
      aria-haspopup={ariaHaspopup.toString()}
      aria-expanded={ariaExpanded.toString()}
    >{children}
    </button>
  );
};

DropdownToggle.defaultProps = {
  className: '',
  style: null,
  children: null,
  href: null,
  intent: '',
  split: false,
  ariaHaspopup: true,
  ariaExpanded: false,
  id: '',
  name: '',
};

DropdownToggle.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  ariaHaspopup: PropTypes.bool,
  ariaExpanded: PropTypes.bool,
  href: PropTypes.string,
  intent: PropTypes.string,
  split: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(DropdownToggle);
