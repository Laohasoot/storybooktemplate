export default {
  active: 'active',
  primary: 'primary',
  secondary: 'secondary',
  danger: 'danger',
  warning: 'warning',
  success: 'success',
  info: 'info',
  light: 'light',
  dark: 'dark',
  muted: 'muted',
  white: 'white',
};

