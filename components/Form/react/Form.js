
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Form = ({
  inline, className, children
  , style, ...props }) => {
  let mutableClassName = '';
  if (inline) mutableClassName = `${mutableClassName} form-inline ${className}`;
  return <form className={mutableClassName} style={style}  {...props} >{children}</form>;
};

Form.defaultProps = {
  className: '',
  style: null,
  children: null,
  inline: false,
};

Form.propTypes = {
  inline: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Form);
