
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const FormCheck = ({ disabled, className, children , style })=> {
  let mutableClassName = `form-check ${className}`;
  if (disabled) mutableClassName = `${mutableClassName} disabled`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

FormCheck.defaultProps = {
  className: '',
  style: null,
  disabled: false,
  children: null,
};

FormCheck.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  disabled: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(FormCheck);
