
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const FormGroup = ({ row, className, children , style })=> {
  let mutableClassName = 'form-group';
  if (row) mutableClassName = `${mutableClassName} row`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

FormGroup.defaultProps = {
  className: '',
  style: null,
  row: false,
  children: null,
};

FormGroup.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  row: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(FormGroup);
