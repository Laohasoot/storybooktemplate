
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const FormRow = ({ className, children , style })=> {
  const mutableClassName = `form-row ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

FormRow.defaultProps = {
  className: '',
  style: null,
  children: null,
};

FormRow.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(FormRow);
