
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const H2 = ({ className, children , style })=> {
  const mutableClassName = `${className}`;
  return <h2 className={mutableClassName} style={style} >{children}</h2>;
};

H2.defaultProps = {
  className: '',
  style: null,
  children: null,
};

H2.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(H2);
