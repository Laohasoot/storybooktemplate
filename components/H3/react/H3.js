
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const H3 = ({ className, children , style })=> {
  const mutableClassName = `${className}`;
  return <h3 className={mutableClassName} style={style} >{children}</h3>;
};

H3.defaultProps = {
  className: '',
  style: null,
  children: null,
};

H3.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(H3);
