
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const H4 = ({ className, children , style })=> {
  const mutableClassName = `${className}`;
  return <h4 className={mutableClassName} style={style} >{children}</h4>;
};

H4.defaultProps = {
  className: '',
  style: null,
  children: null,
};

H4.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(H4);
