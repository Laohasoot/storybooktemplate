
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const H5 = ({ className, children , style })=> {
  const mutableClassName = `${className}`;
  return <h5 className={mutableClassName} style={style} >{children}</h5>;
};

H5.defaultProps = {
  className: '',
  style: null,
  children: null,
};

H5.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(H5);
