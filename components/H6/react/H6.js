
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const H6 = ({ className, children , style })=> {
  const mutableClassName = `${className}`;
  return <h6 className={mutableClassName} style={style} >{children}</h6>;
};

H6.defaultProps = {
  className: '',
  style: null,
  children: null,
};

H6.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(H6);
