
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Input = ({
  lg, sm, type, className
  , style, ...props }) => {
  let mutableClassName = 'form-control';
  if (type === 'file') mutableClassName = 'form-control-file';
  if (lg) mutableClassName = `${mutableClassName} form-control-lg`;
  if (sm) mutableClassName = `${mutableClassName} form-control-sm`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <input className={mutableClassName} style={style}  {...props} />;
};

Input.defaultProps = {
  lg: false,
  sm: false,
  type: 'text',
  className: '',
  style: null,
};

Input.propTypes = {
  lg: PropTypes.bool,
  sm: PropTypes.bool,
  type: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
};


export default UtilComponent(Input);
