
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const InputGroup = ({ className, children , style })=> {
  const mutableClassName = `input-group ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

InputGroup.defaultProps = {
  className: '',
  style: null,
  children: null,
};

InputGroup.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(InputGroup);
