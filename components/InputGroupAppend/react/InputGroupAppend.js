
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const InputGroupAppend = ({ className, children , style })=> {
  const mutableClassName = `input-group-append ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

InputGroupAppend.defaultProps = {
  className: '',
  style: null,
  children: null,
};

InputGroupAppend.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(InputGroupAppend);
