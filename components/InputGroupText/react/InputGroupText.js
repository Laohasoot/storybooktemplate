
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const InputGroupText = ({ className, children , style })=> {
  const mutableClassName = `input-group-text ${className}`;
  return <span className={mutableClassName} style={style}  >{children}</span>;
};

InputGroupText.defaultProps = {
  className: '',
  style: null,
  children: null,
};

InputGroupText.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(InputGroupText);
