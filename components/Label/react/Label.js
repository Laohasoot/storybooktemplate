
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Label = ({
  htmlFor,
  xs,
  sm,
  md,
  lg,
  xl,
  type,
  children, style }) => {
  let mutableClassName = 'col-form-label';
  const colxs = 'col-';
  const colsm = 'col-sm-';
  const colmd = 'col-md-';
  const collg = 'col-lg-';
  const colxl = 'col-xl-';
  const formCheckLabel = 'form-check-label';

  if (xs) mutableClassName = `${colxs}${xs} ${mutableClassName}`;
  if (sm) mutableClassName = `${colsm}${sm} ${mutableClassName}`;
  if (md) mutableClassName = `${colmd}${md} ${mutableClassName}`;
  if (lg) mutableClassName = `${collg}${lg} ${mutableClassName}`;
  if (xl) mutableClassName = `${colxl}${xl} ${mutableClassName}`;
  if (type === 'checkbox') mutableClassName = `${formCheckLabel}`;

  return <label htmlFor={htmlFor} className={mutableClassName} style={style} children={children} />;
};


Label.defaultProps = {
  htmlFor: '',
  xs: null,
  sm: null,
  md: null,
  lg: null,
  xl: null,
  type: 'text',
  children: null,
};

Label.propTypes = {
  htmlFor: PropTypes.string,
  xs: PropTypes.number,
  sm: PropTypes.number,
  md: PropTypes.number,
  lg: PropTypes.number,
  xl: PropTypes.number,
  type: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(Label);
