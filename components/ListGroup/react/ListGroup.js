
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const ListGroup = ({ className, children, flush , style })=> {
  const mutableClassName = flush ? `list-group list-group-flush ${className}` : `list-group ${className}`;
  return <ul className={mutableClassName} style={style}  >{children}</ul>;
};

ListGroup.defaultProps = {
  className: '',
  style: null,
  children: null,
  flush: false,
};

ListGroup.propTypes = {
  flush: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ListGroup);
