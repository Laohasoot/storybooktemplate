
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const ListGroupItem = ({
  className, children, active, disabled, style }) => {
  let mutableClassName = 'list-group-item';
  if (active) mutableClassName = `${mutableClassName} active`;
  if (disabled) mutableClassName = `${mutableClassName} disabled`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <li className={mutableClassName} style={style}  >{children}</li>;
};

ListGroupItem.defaultProps = {
  className: '',
  style: null,
  children: null,
  active: false,
  disabled: false,
};

ListGroupItem.propTypes = {
  disabled: PropTypes.bool,
  active: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ListGroupItem);
