
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Modal = ({
  className, children, style, tabindex, ariaLabelledby, ariaHidden, fade,
  id, name,
}) => {
  let mutableClassName = 'modal';
  if (fade) mutableClassName = `${mutableClassName} fade`;
  mutableClassName = `${mutableClassName} ${className}`;
  return (
    <div
      className={mutableClassName}
      tabIndex={tabindex}
      style={style}
      role="dialog"
      aria-labelledby={ariaLabelledby}
      aria-hidden={ariaHidden}
      id={id}
      name={name}
    >{children}
    </div>
  );
};

Modal.defaultProps = {
  id: '',
  name: '',
  fade: true,
  ariaHidden: '',
  ariaLabelledby: '',
  tabindex: '-1',
  style: null,
  className: '',
  children: null,
};

Modal.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  fade: PropTypes.bool,
  ariaHidden: PropTypes.string,
  ariaLabelledby: PropTypes.string,
  tabindex: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Modal);
