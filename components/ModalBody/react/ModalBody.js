
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const ModalBody = ({
  className, children, style, id, name,
}) => {
  const mutableClassName = `modal-body ${className}`;
  return <div className={mutableClassName} style={style} id={id} name={name} >{children}</div>;
};

ModalBody.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

ModalBody.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ModalBody);
