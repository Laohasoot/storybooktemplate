
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const ModalDialog = ({
  className, children, style, id, name, centered, large, small,
}) => {
  let mutableClassName = `modal-dialog ${className}`;
  if (centered) mutableClassName = `${mutableClassName} modal-dialog-centered`;
  if (large) mutableClassName = `${mutableClassName} modal-lg`;
  if (small) mutableClassName = `${mutableClassName} modal-sm`;
  return <div className={mutableClassName} role="document" style={style} id={id} name={name} >{children}</div>;
};

ModalDialog.defaultProps = {
  centered: false,
  large: false,
  small: false,
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

ModalDialog.propTypes = {
  centered: PropTypes.bool,
  large: PropTypes.bool,
  small: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ModalDialog);
