
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const ModalFooter = ({
  className, children, style, id, name,
}) => {
  const mutableClassName = `modal-footer ${className}`;
  return <div className={mutableClassName} style={style} id={id} name={name} >{children}</div>;
};

ModalFooter.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

ModalFooter.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ModalFooter);
