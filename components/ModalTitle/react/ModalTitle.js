
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const ModalTitle = ({
  className, children, style, id, name,
}) => {
  const mutableClassName = `modal-title ${className}`;
  return <h5 className={mutableClassName} style={style} id={id} name={name} >{children}</h5>;
};

ModalTitle.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

ModalTitle.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(ModalTitle);
