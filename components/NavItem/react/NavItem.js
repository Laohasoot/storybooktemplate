
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const NavItem = ({ className, children , style })=> {
  const mutableClassName = `nav-item ${className}`;
  return <li className={mutableClassName} style={style}  >{children}</li>;
};

NavItem.defaultProps = {
  className: '',
  style: null,
  children: null,
};

NavItem.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavItem);
