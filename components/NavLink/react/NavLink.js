
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const NavLink = ({
  className, active, disabled, children, style }) => {
  const mutableClassName = `nav-link ${active ? 'active' : ''} ${disabled ? 'disabled' : ''} ${className}`;
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

NavLink.defaultProps = {
  className: '',
  style: null,
  children: null,
  active: false,
  disabled: false,
};

NavLink.propTypes = {
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavLink);
