
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Navbar = ({
  className, children, theme, expand, style }) => {
  let mutableClassName = `navbar ${className}`;
  if (expand) mutableClassName = `${mutableClassName} navbar-expand-${expand}`;
  switch (theme) {
    case 'light':
      mutableClassName = `${mutableClassName} navbar-light`;
      break;
    case 'dark':
      mutableClassName = `${mutableClassName} navbar-dark`;
      break;
    default:
      mutableClassName = `${mutableClassName} navbar-light`;
      break;
  }
  return <div className={mutableClassName} style={style}  >{children}</div>;
};

Navbar.defaultProps = {
  theme: 'light',
  expand: 'lg',
  className: '',
  style: null,
  children: null,
};

Navbar.propTypes = {
  theme: PropTypes.string,
  expand: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Navbar);
