
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const NavbarBrand = ({ className, href, children , style })=> {
  const mutableClassName = `navbar-brand ${className}`;
  return <a className={mutableClassName} style={style} href={href}>{children}</a>;
};

NavbarBrand.defaultProps = {
  href: '#',
  className: '',
  style: null,
  children: null,
};

NavbarBrand.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavbarBrand);
