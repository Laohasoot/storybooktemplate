
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const NavbarNav = ({ className, children , style })=> {
  let mutableClassName = 'navbar-nav';
  mutableClassName = `${mutableClassName} ${className}`;
  return <ul className={mutableClassName} style={style}  >{children}</ul>;
};

NavbarNav.defaultProps = {
  className: '',
  style: null,
  children: null,
};

NavbarNav.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavbarNav);
