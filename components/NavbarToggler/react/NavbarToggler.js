
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const NavbarToggler = ({
  className, children, style, ...props
}) => {
  const mutableClassName = `navbar-toggler ${className}`;
  return (
    <button
      type="button"
      className={mutableClassName}
      style={style}
      {...props}
    >{children}
    </button>
  );
};

NavbarToggler.defaultProps = {
  className: '',
  style: null,
  children: null,
};

NavbarToggler.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavbarToggler);
