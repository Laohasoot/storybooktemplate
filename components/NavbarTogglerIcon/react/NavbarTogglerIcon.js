
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const NavbarTogglerIcon = ({ className, children , style })=> {
  const mutableClassName = `navbar-toggler-icon ${className}`;
  return <span className={mutableClassName} style={style}  >{children}</span>;
};

NavbarTogglerIcon.defaultProps = {
  className: '',
  style: null,
  children: null,
};

NavbarTogglerIcon.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(NavbarTogglerIcon);
