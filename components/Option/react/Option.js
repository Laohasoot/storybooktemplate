
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Option = ({
  className,
  children,
  style,
  ...props
}) => {
  const mutableClassName = `${className}`;
  return <option className={mutableClassName} style={style} {...props} >{children}</option>;
};
Option.defaultProps = {
  className: '',
  style: null,
  children: null,
};

Option.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Option);
