
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Radio = ({ className, style, ...props }) => {
  const mutableClassName = `form-check-input ${className}`;
  return (<input className={mutableClassName} style={style} type="radio" {...props} />);
};

Radio.defaultProps = {
  className: '',
  style: null,
};

Radio.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
};

export default UtilComponent(Radio);
