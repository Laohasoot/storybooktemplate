
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Row = ({ className, children , style })=> {
  const mutableClassName = `row ${className}`;
  return (<div className={mutableClassName} style={style} > {children}</div>);
};

Row.defaultProps = {
  className: '',
  style: null,
  children: null,
};

Row.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};


export default UtilComponent(Row);
