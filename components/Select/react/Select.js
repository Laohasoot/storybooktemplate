
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Select = ({
  lg, sm, className, children, style, ...props
}) => {
  let mutableClassName = 'form-control';
  if (lg) mutableClassName = `${mutableClassName} form-control-lg`;
  if (sm) mutableClassName = `${mutableClassName} form-control-sm`;
  mutableClassName = `${mutableClassName} ${className}`;

  return <select className={mutableClassName} style={style} {...props}>{children}</select>;
};

Select.defaultProps = {
  className: '',
  style: null,
  children: null,
  lg: false,
  sm: false,
};

Select.propTypes = {
  lg: PropTypes.bool,
  sm: PropTypes.bool,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Select);
