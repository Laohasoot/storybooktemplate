
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Spinner = ({
  className, style, id, name, intent, grow,
}) => {
  let mutableClassName = 'spinner-border';
  if (grow) mutableClassName = 'spinner-grow';
  if (intent) mutableClassName = `${mutableClassName} text-${intent}`;
  mutableClassName = `${mutableClassName} ${className}`;
  return (
    <div className={mutableClassName} role="status" style={style} id={id} name={name}>
      <span className="sr-only">Loading...</span>
    </div>
  );
};

Spinner.defaultProps = {
  grow: false,
  intent: '',
  id: '',
  name: '',
  style: null,
  className: '',
};

Spinner.propTypes = {
  grow: PropTypes.bool,
  intent: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
};

export default UtilComponent(Spinner);
