
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Table = ({
  className, children, style, id, name, dark, striped, bordered, hoverable, borderless,
}) => {
  let mutableClassName = `table ${className}`;
  if (dark) mutableClassName = `${mutableClassName} table-dark`;
  if (striped) mutableClassName = `${mutableClassName} table-striped`;
  if (bordered) mutableClassName = `${mutableClassName} table-bordered`;
  if (hoverable) mutableClassName = `${mutableClassName} table-hover`;
  if (borderless) mutableClassName = `${mutableClassName} table-borderless`;
  return <table className={mutableClassName} style={style} id={id} name={name} >{children}</table>;
};

Table.defaultProps = {
  hoverable: false,
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
  dark: false,
  striped: false,
  bordered: false,
  borderless: false,
};

Table.propTypes = {
  borderless: PropTypes.bool,
  hoverable: PropTypes.bool,
  bordered: PropTypes.bool,
  dark: PropTypes.bool,
  striped: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Table);
