
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const TableBody = ({
  className, children, style, id, name,
}) => {
  const mutableClassName = `${className}`;
  return <tbody className={mutableClassName} style={style} id={id} name={name} >{children}</tbody>;
};

TableBody.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

TableBody.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(TableBody);
