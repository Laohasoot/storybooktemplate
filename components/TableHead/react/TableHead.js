
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const TableHead = ({
  className, children, style, id, name, dark, light,
}) => {
  let mutableClassName = '';
  if (dark) mutableClassName = `${mutableClassName} thead-dark `;
  if (light) mutableClassName = `${mutableClassName} thead-light `;
  mutableClassName = `${mutableClassName} ${className}`;
  return <thead className={mutableClassName} style={style} id={id} name={name} >{children}</thead>;
};

TableHead.defaultProps = {
  light: false,
  dark: false,
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

TableHead.propTypes = {
  light: PropTypes.bool,
  dark: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(TableHead);
