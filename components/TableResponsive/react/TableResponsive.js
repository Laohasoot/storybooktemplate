
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const TableResponsive = ({
  className, children, style, id, name, small, medium, large, extraLarge,
}) => {
  let mutableClassName = 'table-responsive';
  if (small) mutableClassName = `${mutableClassName}-sm`;
  if (medium) mutableClassName = `${mutableClassName}-md`;
  if (large) mutableClassName = `${mutableClassName}-lg`;
  if (extraLarge) mutableClassName = `${mutableClassName}-xl`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <div className={mutableClassName} style={style} id={id} name={name} >{children}</div>;
};

TableResponsive.defaultProps = {
  small: false,
  medium: false,
  large: false,
  extraLarge: false,
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

TableResponsive.propTypes = {
  small: PropTypes.bool,
  medium: PropTypes.bool,
  large: PropTypes.bool,
  extraLarge: PropTypes.bool,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(TableResponsive);
