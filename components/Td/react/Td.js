
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Td = ({
  className, children, style, id, name, colspan, intent,
}) => {
  let mutableClassName = '';
  if (intent) mutableClassName = `${mutableClassName} table-${intent}`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <td className={mutableClassName} colSpan={colspan} style={style} id={id} name={name} >{children}</td>;
};

Td.defaultProps = {
  intent: null,
  colspan: '',
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

Td.propTypes = {
  intent: PropTypes.string,
  colspan: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Td);
