
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';


const Text = ({
  children, intent, italic, fontWeight, className, style,
}) => {
  let mutableClassName = className;
  if (italic) mutableClassName = `${mutableClassName} font-italic`;
  if (fontWeight) mutableClassName = `${mutableClassName} font-weight-${fontWeight}`;
  if (intent) mutableClassName = `${mutableClassName} text-${intent}`;
  return <p className={mutableClassName} style={style} >{children}</p>;
};

Text.defaultProps = {
  intent: null,
  className: '',
  style: null,
  children: null,
  fontWeight: null,
  italic: null,
};

Text.propTypes = {
  italic: PropTypes.bool,
  fontWeight: PropTypes.string,
  intent: PropTypes.string,
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Text);
