
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import UtilComponent from '../../UtilComponent';

const Textarea = ({
  className, children, style, ...props
}) => {
  const mutableClassName = `form-control ${className}`;
  return <textarea className={mutableClassName} style={style} {...props} >{children}</textarea>;
};

Textarea.defaultProps = {
  className: '',
  style: null,
  children: null,
};

Textarea.propTypes = {
  className: PropTypes.string,
  style: stylePropType,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Textarea);
