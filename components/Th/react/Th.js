
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Th = ({
  className, children, style, id, name, scope,
}) => {
  const mutableClassName = `${className}`;
  return <th className={mutableClassName} scope={scope} style={style} id={id} name={name} >{children}</th>;
};

Th.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
  scope: '',
};

Th.propTypes = {
  scope: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Th);
