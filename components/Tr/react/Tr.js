
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const Tr = ({
  className, children, style, id, name, intent,
}) => {
  let mutableClassName = '';
  if (intent) mutableClassName = `${mutableClassName} table-${intent}`;
  mutableClassName = `${mutableClassName} ${className}`;
  return <tr className={mutableClassName} style={style} id={id} name={name} >{children}</tr>;
};

Tr.defaultProps = {
  intent: null,
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

Tr.propTypes = {
  intent: PropTypes.string,
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(Tr);
