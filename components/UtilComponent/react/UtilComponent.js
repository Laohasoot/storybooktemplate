
import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import propToBootstrapClass from './UtilComponent.bootstrapClass';
import { propTypes, defaultProps } from './UtilComponent.propTypes';
import '../UtilComponent.css';

const addBootstrapClass = (mutableClassName, key, className) => {
  const bootstrapClassName = className || propToBootstrapClass[key];
  if (key) {
    return `${mutableClassName} ${bootstrapClassName}`;
  }
  return mutableClassName;
};

const UtilComponent = (WrapCompoennt) => {
  const Util = ({
    marginTop,
    marginBottom,
    marginLeft,
    marginRight,
    order,
    orderSmall,
    orderMedium,
    orderLarge,
    orderExtraLarge,
    className,
    textIntent,
    bgIntent,
    bgGradientIntent,
    padding,
    width,
    height,
    style,
    ...props
  }) => {
    let mutableClassName = '';
    mutableClassName = addBootstrapClass(mutableClassName, marginTop, `mt-${marginTop}`);
    mutableClassName = addBootstrapClass(mutableClassName, marginBottom, `mb-${marginBottom}`);
    mutableClassName = addBootstrapClass(mutableClassName, marginLeft, `ml-${marginLeft}`);
    mutableClassName = addBootstrapClass(mutableClassName, marginRight, `mr-${marginRight}`);
    mutableClassName = addBootstrapClass(mutableClassName, order, `order-${order}`);
    mutableClassName = addBootstrapClass(mutableClassName, orderSmall, `order-${orderSmall}`);
    mutableClassName = addBootstrapClass(mutableClassName, orderMedium, `order-${orderMedium}`);
    mutableClassName = addBootstrapClass(mutableClassName, orderLarge, `order-${orderLarge}`);
    mutableClassName = addBootstrapClass(mutableClassName, orderExtraLarge, `order-${orderExtraLarge}`);
    mutableClassName = addBootstrapClass(mutableClassName, textIntent, `text-${textIntent}`);
    mutableClassName = addBootstrapClass(mutableClassName, bgIntent, `bg-${bgIntent}`);
    mutableClassName = addBootstrapClass(mutableClassName, bgGradientIntent, `bg-gradient-${bgGradientIntent}`);
    mutableClassName = addBootstrapClass(mutableClassName, padding, `p-${padding}`);

    Object.keys(props).forEach((key) => {
      if (propToBootstrapClass[key] && props[key]) {
        mutableClassName = addBootstrapClass(mutableClassName, key);
      }
    });

    mutableClassName = `${mutableClassName} ${className}`.trim();
    return <WrapCompoennt className={mutableClassName} style={{ width, height, ...style }} {...props} />;
  };

  Util.defaultProps = {
    className: '',
    style: null,
    children: null,
    ...defaultProps,
    ...WrapCompoennt.defaultProps,
  };

  Util.propTypes = {
    className: PropTypes.string,
    style: stylePropType,
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    ...propTypes,
    ...WrapCompoennt.propTypes,
  };

  return Util;
};


export default UtilComponent;
