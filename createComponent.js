const prompt = require('prompt');
const fs = require('fs');
const genIndexFile = require('./genIndex');

const schema = {
  properties: {
    componentName: {
      required: true,
    },
  },
};

const indexTemplate = componentName => `
  import ${componentName} from './react/${componentName}'

  export default ${componentName}
`;

const reactTemplate = componentName => `
import React from 'react'
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';

import UtilComponent from '../../UtilComponent';


const ${componentName} = ({ className, children, style, id, name }) => {
  const mutableClassName = "" + className
  return <div className={mutableClassName} style={style} id={id} name={name}  >{children}</div>
}

${componentName}.defaultProps = {
  id: '',
  name: '',
  style: null,
  className: '',
  children: null,
};

${componentName}.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  style: stylePropType,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UtilComponent(${componentName})
`;

const storyTemplate = componentName => `
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ${componentName} from '../components/${componentName}';

storiesOf('${componentName}', module)
  .addDecorator(withKnobs)
  .add('simple ${componentName}', () => (
    <${componentName}>
       {text('chrild field','Hello ${componentName}')}
    </${componentName}> 
   )
  )
`;


prompt.start();

prompt.get(schema, async (e, result) => {
  if (e) throw e;
  try {
    console.log('Component will create :');
    console.log(`  name: ${result.componentName}`);
    const { componentName } = result;
    const rootPath = `./components/${componentName}`;
    await fs.promises.mkdir(`${rootPath}`, { recursive: true });
    await fs.promises.mkdir(`${rootPath}/react`, { recursive: true });
    await fs.promises.writeFile(`${rootPath}/index.js`, indexTemplate(result.componentName));
    await fs.promises.writeFile(`${rootPath}/${componentName}.css`, '');
    await fs.promises.writeFile(`${rootPath}/react/${componentName}.js`, reactTemplate(result.componentName));
    await fs.promises.writeFile(`${rootPath}/react/${componentName}.test.js`, '');
    await fs.promises.writeFile(`./stories/${componentName}.stories.js`, storyTemplate(result.componentName));
    console.log('create Component complete');
    await genIndexFile();
  } catch (e) {
    throw e;
  }
});
