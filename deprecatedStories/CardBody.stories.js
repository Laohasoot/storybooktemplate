
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardBody from '../components/CardBody';

storiesOf('Components|CardBody', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardBody>
      {text('chrild field', 'Hello CardBody')}
    </CardBody>
  ));
