
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardFooter from '../components/CardFooter';

storiesOf('Components|CardFooter', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardFooter>
      {text('chrild field', 'Hello CardFooter')}
    </CardFooter>
  ));
