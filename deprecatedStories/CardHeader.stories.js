
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardHeader from '../components/CardHeader';

storiesOf('Components|CardHeader', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardHeader>
      {text('chrild field', 'Hello CardHeader')}
    </CardHeader>
  ));
