
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardHeaderTab from '../components/CardHeaderTab';

storiesOf('Components|CardHeaderTab', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardHeaderTab>
      {text('chrild field', 'Hello CardHeaderTab')}
    </CardHeaderTab>
  ));
