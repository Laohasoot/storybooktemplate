
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardImageTop from '../components/CardImageTop';

storiesOf('Components|CardImageTop', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardImageTop>
      {text('chrild field', 'Hello CardImageTop')}
    </CardImageTop>
  ));
