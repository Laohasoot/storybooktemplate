
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardLink from '../components/CardLink';

storiesOf('Components|CardLink', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardLink>
      {text('chrild field', 'Hello CardLink')}
    </CardLink>
  )
  )
