
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardSubtitle from '../components/CardSubtitle';

storiesOf('Components|CardSubtitle', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardSubtitle>
      {text('chrild field', 'Hello CardSubtitle')}
    </CardSubtitle>
  ));
