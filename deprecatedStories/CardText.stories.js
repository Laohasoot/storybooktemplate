
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardText from '../components/CardText';

storiesOf('Components|CardText', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardText>
      {text('chrild field', 'Hello CardText')}
    </CardText>
  ));
