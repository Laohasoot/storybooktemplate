
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import CardTitle from '../components/CardTitle';

storiesOf('Components|CardTitle', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <CardTitle>
      {text('chrild field', 'Hello CardTitle')}
    </CardTitle>
  ));
