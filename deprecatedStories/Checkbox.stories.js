
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Checkbox from '../components/Checkbox';

storiesOf('Components|Checkbox', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Checkbox />
  ));
