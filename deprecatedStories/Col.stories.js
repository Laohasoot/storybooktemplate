
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Col from '../components/Col';
import Row from '../components/Row';
import Container from '../components/Container';

storiesOf('Layout|Col', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Container>
      <Row>
        <Col bgIntent={text('bgIntent', 'primary')} textIntent={text('textIntent', 'light')} textCenter >
          {text('chrild field', 'Hello Col')}
        </Col>
        <Col bgIntent="dark" textIntent="light">
          {text('chrild field', 'Hello Col')}
        </Col>
        <Col bgIntent="warning" textIntent="light">
          {text('chrild field', 'Hello Col')}
        </Col>
        <Col bgIntent="danger" textIntent="light">
          {text('chrild field', 'Hello Col')}
        </Col>
      </Row>
    </Container >
  ));
