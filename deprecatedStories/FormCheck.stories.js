
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import FormCheck from '../components/FormCheck';

storiesOf('Components|FormCheck', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <FormCheck>
      {text('chrild field', 'Hello FormCheck')}
    </FormCheck>
  ));
