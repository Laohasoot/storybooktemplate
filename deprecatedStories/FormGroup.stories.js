
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import FormGroup from '../components/FormGroup';

storiesOf('Components|FormGroup', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <FormGroup>
      {text('chrild field', 'Hello FormGroup')}
    </FormGroup>
  ));
