
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import FormRow from '../components/FormRow';

storiesOf('Components|FormRow', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <FormRow>
      {text('chrild field', 'Hello FormRow')}
    </FormRow>
  ));
