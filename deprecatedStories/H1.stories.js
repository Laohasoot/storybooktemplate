
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';

import H1 from '../components/H1';

storiesOf('Components|H1', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H1>
      {text('chrild field', 'Hello H1')}
    </H1>
  ));
