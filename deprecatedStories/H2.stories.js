
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import H2 from '../components/H2';

storiesOf('Components|H2', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H2>
      {text('chrild field', 'Hello H2')}
    </H2>
  ));
