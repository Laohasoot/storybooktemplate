
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import H3 from '../components/H3';

storiesOf('Components|H3', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H3>
      {text('chrild field', 'Hello H3')}
    </H3>
  ));
