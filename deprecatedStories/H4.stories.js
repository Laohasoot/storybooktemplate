
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import H4 from '../components/H4';

storiesOf('Components|H4', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H4>
      {text('chrild field', 'Hello H4')}
    </H4>
  ));
