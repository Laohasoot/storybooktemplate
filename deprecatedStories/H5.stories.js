
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import H5 from '../components/H5';

storiesOf('Components|H5', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H5>
      {text('chrild field', 'Hello H5')}
    </H5>
  ));
