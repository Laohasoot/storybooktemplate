
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import H6 from '../components/H6';

storiesOf('Components|H6', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <H6>
      {text('chrild field', 'Hello H6')}
    </H6>
  ));
