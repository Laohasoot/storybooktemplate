
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import InputGroupAppend from '../components/InputGroupAppend';

storiesOf('Components|InputGroupAppend', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <InputGroupAppend>
      {text('chrild field', 'Hello InputGroupAppend')}
    </InputGroupAppend>
  ));
