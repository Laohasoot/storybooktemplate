
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import InputGroupPrepend from '../components/InputGroupPrepend';

storiesOf('Components|InputGroupPrepend', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <InputGroupPrepend>
      {text('chrild field', 'Hello InputGroupPrepend')}
    </InputGroupPrepend>
  ));
