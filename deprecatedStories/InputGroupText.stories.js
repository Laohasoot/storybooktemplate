
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import InputGroupText from '../components/InputGroupText';

storiesOf('Components|InputGroupText', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <InputGroupText>
      {text('chrild field', 'Hello InputGroupText')}
    </InputGroupText>
  ));
