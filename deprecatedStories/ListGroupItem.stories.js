
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ListGroupItem from '../components/ListGroupItem';

storiesOf('Components|ListGroupItem', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <ListGroupItem>
      {text('chrild field', 'Hello ListGroupItem')}
    </ListGroupItem>
  ));
