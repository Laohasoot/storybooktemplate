
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavItem from '../components/NavItem';

storiesOf('Components|NavItem', module)
  .addDecorator(withKnobs).add('simple component', () => (
    <NavItem>
      {text('chrild field', 'Hello NavItem')}
    </NavItem>
  ));
