
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavLink from '../components/NavLink';

storiesOf('Components|NavLink', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <NavLink>
      {text('chrild field', 'Hello NavLink')}
    </NavLink>
  ));
