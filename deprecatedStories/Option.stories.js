
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Option from '../components/Option';

storiesOf('Components|Option', module)
  .addDecorator(withKnobs)
  .add('with text', () => (
    <Option>
      {text('chrild field', 'Hello Option')}
    </Option>
  ));
