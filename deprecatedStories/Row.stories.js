
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Row from '../components/Row';
import Container from '../components/Container';

storiesOf('Layout|Row', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Container>
      <Row>
        {text('chrild field1', 'Hello Row')}
      </Row>
      <Row>
        {text('chrild field2', 'Hello Row')}
      </Row>
    </Container>
  ));
