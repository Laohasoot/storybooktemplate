const fs = require('fs');

const rootPath = './components';


const indexTemplate = (dir) => {
    const componentNameList = dir.filter(folderName => folderName !== 'index.js' && folderName !== 'Enum');
    const importComponentTemplate = componentNameList.map(componentName => `import ${componentName} from './${componentName}';
`);

    const exportComponentTemplate = componentNameList.map(componentName => `${componentName},
  `);

    return `
import 'bootstrap/dist/css/bootstrap.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import Intent from './Enum/intent';

${importComponentTemplate.join('')}

export {
  Intent,
  ${exportComponentTemplate.join('')}
};

export default {
  Intent,
  ${exportComponentTemplate.join('')}
};
`;
};

const genIndexFile = async () => {
    const dir = await fs.promises.readdir(`${rootPath}`);
    await fs.promises.writeFile(`${rootPath}/index.js`, indexTemplate(dir));
    console.log('Gen new index file complete');
};

module.exports = genIndexFile;

