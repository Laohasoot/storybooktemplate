
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Alert from '../components/Alert';
import { Container } from '../components';
import Row from '../components/Row';
import Col from '../components/Col';

storiesOf('Components|Alert', module)
  .addDecorator(withKnobs)
  .add('simple alert', () => (
    <Container>
      <Row>
        <Col>
          <Alert intent="primary">
            This is a primary alert—check it out!
          </Alert>
          <Alert intent="secondary">
            This is a secondary alert—check it out!
          </Alert>
          <Alert intent="success">
            This is a success alert—check it out!
          </Alert>
          <Alert intent="danger">
            This is a danger alert—check it out!
          </Alert>
          <Alert intent="warning">
            This is a warning alert—check it out!
          </Alert>
          <Alert intent="info">
            This is a info alert—check it out!
          </Alert>
          <Alert intent="light">
            This is a light alert—check it out!
          </Alert>
          <Alert intent="dark">
            This is a dark alert—check it out!
          </Alert>
        </Col>
      </Row>
    </Container>
  ));
