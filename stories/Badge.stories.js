
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import Badge from '../components/Badge';
import { Container } from '../components';
import Row from '../components/Row';
import Col from '../components/Col';
import H1 from '../components/H1';
import H2 from '../components/H2';
import H3 from '../components/H3';
import H4 from '../components/H4';
import H5 from '../components/H5';
import H6 from '../components/H6';
import Button from '../components/Button';


storiesOf('Components|Badge', module)
  .addDecorator(withKnobs)
  .add('simple Badge', () => (
    <Container>
      <Row>
        <Col>
          <H1>Example heading <Badge>New</Badge></H1>
          <H2>Example heading <Badge>New</Badge></H2>
          <H3>Example heading <Badge>New</Badge></H3>
          <H4>Example heading <Badge>New</Badge></H4>
          <H5>Example heading <Badge>New</Badge></H5>
          <H6>Example heading <Badge>New</Badge></H6>
        </Col>
      </Row>
    </Container>
  ))
  .add('Badge in the button', () => (
    <Container>
      <Row>
        <Col>
          <Button>Notifications <Badge intent="light">4</Badge></Button>
        </Col>
      </Row>
    </Container>
  ))
  .add('Contextual variations of Badge', () => (
    <Container>
      <Row>
        <Col md={6}>
          <div className="mt-3 mb-3 text-center">
            <H3><Badge intent={text('intent', 'secondary')}>Example</Badge></H3>
          </div>
          <div className="d-flex flex-wrap justify-content-between">
            <Badge intent="primary">primary</Badge>
            <Badge intent="secondary">secondary</Badge>
            <Badge intent="success">success</Badge>
            <Badge intent="danger">danger</Badge>
            <Badge intent="warning">warning</Badge>
            <Badge intent="info">info</Badge>
            <Badge intent="light">light</Badge>
            <Badge intent="dark">dark</Badge>
          </div>
        </Col>
      </Row>
    </Container>
  ))
  .add('Pill Badge', () => (
    <Container>
      <Row>
        <Col md={6}>
          <div className="mt-3 mb-3 text-center">
            <H3><Badge pill={boolean('pill', true)} intent={text('intent', 'secondary')}>Example</Badge></H3>
          </div>
          <div className="d-flex flex-wrap justify-content-between">
            <Badge pill intent="primary">primary</Badge>
            <Badge pill intent="secondary">secondary</Badge>
            <Badge pill intent="success">success</Badge>
            <Badge pill intent="danger">danger</Badge>
            <Badge pill intent="warning">warning</Badge>
            <Badge pill intent="info">info</Badge>
            <Badge pill intent="light">light</Badge>
            <Badge pill intent="dark">dark</Badge>
          </div>
        </Col>
      </Row>
    </Container>
  ))
  .add('Link Badge', () => (
    <Container>
      <Row>
        <Col md={6}>
          <div className="mt-3 mb-3 text-center">
            <H3><Badge href={text('href', '#')} intent={text('intent', 'secondary')}>Example</Badge></H3>
          </div>
          <div className="d-flex flex-wrap justify-content-between">
            <Badge href="#" intent="primary">primary</Badge>
            <Badge href="#" intent="secondary">secondary</Badge>
            <Badge href="#" intent="success">success</Badge>
            <Badge href="#" intent="danger">danger</Badge>
            <Badge href="#" intent="warning">warning</Badge>
            <Badge href="#" intent="info">info</Badge>
            <Badge href="#" intent="light">light</Badge>
            <Badge href="#" intent="dark">dark</Badge>
          </div>
        </Col>
      </Row>
    </Container>
  ));
