
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ButtonGroup from '../components/ButtonGroup';

storiesOf('Components|ButtonGroup', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <ButtonGroup>
      {text('chrild field', 'Hello ButtonGroup')}
    </ButtonGroup>
  )
  )
