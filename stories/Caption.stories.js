
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Caption from '../components/Caption';

storiesOf('Caption', module)
  .addDecorator(withKnobs)
  .add('simple Caption', () => (
    <Caption>
       {text('chrild field','Hello Caption')}
    </Caption> 
   )
  )
