
import React from 'react';


import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import Card from '../components/Card';
import CardImageTop from '../components/CardImageTop';
import CardBody from '../components/CardBody';
import CardTitle from '../components/CardTitle';
import CardSubtitle from '../components/CardSubtitle';
import CardText from '../components/CardText';
import CardHeader from '../components/CardHeader';
import CardFooter from '../components/CardFooter';
import Container from '../components/Container';
import Row from '../components/Row';
import Col from '../components/Col';
import Button from '../components/Button';
import CardHeaderTab from '../components/CardHeaderTab';
import NavItem from '../components/NavItem';
import NavLink from '../components/NavLink';
import CardLink from '../components/CardLink';
import ListGroup from '../components/ListGroup';
import ListGroupItem from '../components/ListGroupItem';

storiesOf('Components|Card', module)
  .addDecorator(withKnobs)
  .add('simple Card', () => (
    <Container className="mt-5">
      <Row>
        <Col md={5}>
          <Card>
            <CardImageTop src="https://timedotcom.files.wordpress.com/2018/12/lion-conservators-center.jpg?quality=85" alt="Card image cap" />
            <CardBody>
              <CardTitle>Card Title</CardTitle>
              <CardSubtitle className="mb-2 text-muted" >Card Sub Title</CardSubtitle>
              <CardText>
                Some quick example text to build on the card
                title and make up the bulk of the card s content.
              </CardText>
              <Button>Go To Somewhere</Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container >
  ))
  .add('header and footer Card', () => (
    <Container className="mt-5" clasName="something">
      <Row>
        <Col md={5}>
          <Card >
            <CardHeader>Card Header</CardHeader>
            <CardBody>
              <CardTitle>Card Title</CardTitle>
              <CardSubtitle className="mb-2 text-muted" >Card Sub Title</CardSubtitle>
              <CardText>
                Some quick example text to build on the card
                title and make up the bulk of the card s content.
              </CardText>
              <Button>Go To Somewhere</Button>
            </CardBody>
            <CardFooter>Card Footer</CardFooter>
          </Card>
        </Col>
      </Row>
    </Container >
  ))
  .add('link in a Card', () => (
    <Container className="mt-5" clasName="something">
      <Row>
        <Col md={5}>
          <Card >
            <CardHeader>Card Header</CardHeader>
            <CardBody>
              <CardTitle>Card Title</CardTitle>
              <CardSubtitle className="mb-2 text-muted" >Card Sub Title</CardSubtitle>
              <CardText>
                Some quick example text to build on the card
                title and make up the bulk of the card s content.
              </CardText>
              <CardLink href="#">Link one </CardLink><CardLink href="#">Link two </CardLink>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container >
  ))
  .add('List Group Card', () => (
    <Container className="mt-5" clasName="something">
      <Row>
        <Col md={5}>
          <Card >
            <CardImageTop src="https://www.irishcentral.com/uploads/article/130328/cropped_MI_snake_green_getty.jpg?t=1546706211" alt="Card image cap" />
            <CardBody>
              <CardTitle>Hello World !</CardTitle>
              <CardText>
                Some quick example text to build on the card title
                and make up the bulk of the card s content.
              </CardText>
            </CardBody>
            <ListGroup flush>
              <ListGroupItem>Cras justo odio</ListGroupItem>
              <ListGroupItem>Dapibus ac facilisis in</ListGroupItem>
              <ListGroupItem>Vestibulum at eros</ListGroupItem>
            </ListGroup>
            <CardBody>
              <CardLink>Link One</CardLink><CardLink>Link Two</CardLink>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container >
  ))
  .add('Navigation Card', () => (
    <Container className="mt-5">
      <Row>
        <Col md={6}>
          <Card >
            <CardHeader>
              <CardHeaderTab className="test-class">
                <NavItem>
                  <NavLink active >test1</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink >test2</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink disabled >test3</NavLink>
                </NavItem>
              </CardHeaderTab>
            </CardHeader>
            <CardBody>
              <CardTitle>Card Title</CardTitle>
              <CardSubtitle className="mb-2 text-muted" >Card Sub Title</CardSubtitle>
              <CardText>
                Some quick example text to build on the card
                title and make up the bulk of the card s content.
              </CardText>
              <Button>Go To Somewhere</Button>
            </CardBody>
            <CardFooter>Card Footer</CardFooter>
          </Card>
        </Col>
        <Col md={6}>
          <Card >
            <CardHeader>
              <CardHeaderTab pills className="test-class">
                <NavItem>
                  <NavLink active >test1</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink >test2</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink disabled >test3</NavLink>
                </NavItem>
              </CardHeaderTab>
            </CardHeader>
            <CardBody>
              <CardTitle>Card Title</CardTitle>
              <CardSubtitle className="mb-2 text-muted" >Card Sub Title</CardSubtitle>
              <CardText>
                Some quick example text to build on the card
                title and make up the bulk of the card s content.
              </CardText>
              <Button>Go To Somewhere</Button>
            </CardBody>
            <CardFooter>Card Footer</CardFooter>
          </Card>
        </Col>
      </Row>
    </Container >
  ));
