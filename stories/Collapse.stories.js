
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Collapse from '../components/Collapse';

storiesOf('Components|Collapse', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Collapse>
      {text('chrild field', 'Hello Collapse')}
    </Collapse>
  )
  )
