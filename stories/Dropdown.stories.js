
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Dropdown from '../components/Dropdown';
import DropdownToggle from '../components/DropdownToggle';
import DropdownMenu from '../components/DropdownMenu';
import DropdownItem from '../components/DropdownItem';
import { Container, Button } from '../components';
import Row from '../components/Row';
import Col from '../components/Col';
import ButtonGroup from '../components/ButtonGroup/react/ButtonGroup';

storiesOf('Components|Dropdown', module)
  .addDecorator(withKnobs)
  .add('simple Dropdown', () => (
    <Container>
      <Row>
        <Col>
          <Dropdown direction={text('direction', 'down')}>
            <DropdownToggle
              intent="primary"
              id="dropdownMenuButton"
            >Dropdown Button
            </DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton" position={text('position', 'top')}>
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Col>
      </Row>
    </Container>
  ))
  .add('Dropdown with intent', () => (
    <Container>
      <Row>
        <Col flex>
          <Dropdown>
            <DropdownToggle intent="primary" id="dropdownMenuButton">primary</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle intent="secondary" id="dropdownMenuButton">secondary</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle intent="success" id="dropdownMenuButton">success</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle intent="info" id="dropdownMenuButton">info</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle intent="warning" id="dropdownMenuButton">warning</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown>
            <DropdownToggle intent="danger" id="dropdownMenuButton">danger</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Col>
      </Row>
    </Container>
  ))
  .add('split dropdown button', () => (
    <Container>
      <Row>
        <Col flex>
          <ButtonGroup >
            <Button intent="primary" >primary</Button>
            <DropdownToggle intent="primary" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
          <ButtonGroup >
            <Button intent="secondary" >secondary</Button>
            <DropdownToggle intent="secondary" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
          <ButtonGroup >
            <Button intent="success" >success</Button>
            <DropdownToggle intent="success" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
          <ButtonGroup >
            <Button intent="info" >info</Button>
            <DropdownToggle intent="info" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
          <ButtonGroup >
            <Button intent="warning" >warning</Button>
            <DropdownToggle intent="warning" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
          <ButtonGroup >
            <Button intent="danger" >danger</Button>
            <DropdownToggle intent="danger" id="dropdownMenuButton" split />
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </ButtonGroup>
        </Col>
      </Row>
    </Container>
  ))
  .add('Dropdown change direction', () => (
    <Container>
      <Row>
        <Col flex >
          <Dropdown direction="up">
            <DropdownToggle intent="primary" id="dropdownMenuButton">primary</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown direction="left">
            <DropdownToggle intent="secondary" id="dropdownMenuButton">secondary</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown direction="right">
            <DropdownToggle intent="success" id="dropdownMenuButton">success</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
          <Dropdown direction="down">
            <DropdownToggle intent="info" id="dropdownMenuButton">info</DropdownToggle>
            <DropdownMenu ariaLabelledby="dropdownMenuButton">
              <DropdownItem href="#">Action</DropdownItem>
              <DropdownItem href="#">Another action</DropdownItem>
              <DropdownItem href="#">Something else here</DropdownItem>
            </DropdownMenu>
          </Dropdown>
        </Col>
      </Row>
    </Container>
  ));

