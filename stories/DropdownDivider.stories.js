
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import DropdownDivider from '../components/DropdownDivider';

storiesOf('Components|DropdownDivider', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <DropdownDivider>
      {text('chrild field', 'Hello DropdownDivider')}
    </DropdownDivider>
  )
  )
