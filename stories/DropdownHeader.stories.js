
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import DropdownHeader from '../components/DropdownHeader';

storiesOf('Components|DropdownHeader', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <DropdownHeader>
      {text('chrild field', 'Hello DropdownHeader')}
    </DropdownHeader>
  ));
