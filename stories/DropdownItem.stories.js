
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import DropdownItem from '../components/DropdownItem';

storiesOf('Components|DropdownItem', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <DropdownItem>
      {text('chrild field', 'Hello DropdownItem')}
    </DropdownItem>
  ));
