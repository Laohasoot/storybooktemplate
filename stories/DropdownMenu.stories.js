
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import DropdownMenu from '../components/DropdownMenu';

storiesOf('Components|DropdownMenu', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <DropdownMenu>
      {text('chrild field', 'Hello DropdownMenu')}
    </DropdownMenu>
  ));
