
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import DropdownToggle from '../components/DropdownToggle';

storiesOf('Components|DropdownToggle', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <DropdownToggle>
      {text('chrild field', 'Hello DropdownToggle')}
    </DropdownToggle>
  ));
