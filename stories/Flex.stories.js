
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Box from '../components/Box';
import { Container } from '../components';
import Row from '../components/Row';
import H5 from '../components/H5';

const range = length => Array.from({ length }, (v, k) => k + 1);

const createExmapleBoxs = length => range(length).map(number => (
  <Box padding={2} textIntent="dark" bgIntent="highlight" >
    {`flex item ${number}`}
  </Box>
));

storiesOf('Utility|Flex', module)
  .addDecorator(withKnobs)
  .add('flex row and reverse', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex Row</H5>
      </Row>
      <Row bgIntent="highlight" flex flexRow marginTop={3}>
        <Box padding={2} textIntent="dark" bgIntent="highlight" >
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex Row Reverse</H5>
      </Row>
      <Row bgIntent="highlight" flex flexRowReverse marginTop={3} >
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex column and reverse', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex Column</H5>
      </Row>
      <Row bgIntent="highlight" flex flexColumn marginBottom={3} marginTop={3}>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex Column Reverse</H5>
      </Row>
      <Row bgIntent="highlight" flex flexColumnReverse >
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex justify content', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex justifyContentStart</H5>
      </Row>
      <Row bgIntent="highlight" flex justifyContentStart marginTop={3}>
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex justifyContentEnd marginTop={3} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex justifyContentCenter marginTop={3} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentBetween</H5>
      </Row>
      <Row bgIntent="highlight" flex justifyContentBetween marginTop={3} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentAround</H5>
      </Row>
      <Row bgIntent="highlight" flex justifyContentAround marginTop={3} >
        {createExmapleBoxs(6)}
      </Row>
    </Container>
  ))
  .add('flex align content', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex justifyContentStart</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentStart flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentEnd flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentCenter flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentBetween</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentBetween flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentAround</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentAround flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex justifyContentAround</H5>
      </Row>
      <Row bgIntent="highlight" flex alignContentStretch flexWrap marginTop={3} height={200} >
        {createExmapleBoxs(20)}
      </Row>
    </Container>
  ))
  .add('flex Align items', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex alignItemsStart</H5>
      </Row>
      <Row bgIntent="highlight" flex alignItemsStart marginTop={3} height={100} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignItemsEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex alignItemsEnd marginTop={3} height={100} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignItemsCenter</H5>
      </Row>
      <Row bgIntent="highlight" flex alignItemsCenter marginTop={3} height={100} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignItemsBaseline</H5>
      </Row>
      <Row bgIntent="highlight" flex alignItemsBaseline marginTop={3} height={100} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignItemsStretch</H5>
      </Row>
      <Row bgIntent="highlight" flex alignItemsStretch marginTop={3} height={100} >
        {createExmapleBoxs(6)}
      </Row>
    </Container>
  ))
  .add('flex Align Self', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex alignSelfStart</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} height={100} >
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStart>
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStart>
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStart>
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignSelfEnd</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} height={100} >
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfEnd>
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfEnd>
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfEnd>
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignSelfCenter</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} height={100} >
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfCenter>
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfCenter>
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfCenter>
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignSelfBaseline</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} height={100} >
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfBaseline>
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfBaseline>
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfBaseline>
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex alignSelfStretch</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} height={100} >
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStretch>
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStretch>
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight" alignSelfStretch>
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex with auto margin', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex Normal</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3}>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex with marginRightAuto in item 1</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} >
        <Box padding={2} marginRightAuto textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex with marginLeftAuto in item 3</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} >
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} marginLeftAuto textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex with alignItems and MarginAuto', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex with marginBottomAuto in item 1</H5>
      </Row>
      <Row bgIntent="highlight" flex flexColumn alignItemsStart marginTop={3} height={200}>
        <Box marginBottomAuto padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex with marginTopAuto in item 3</H5>
      </Row>
      <Row bgIntent="highlight" flex flexColumn alignItemsEnd marginTop={3} height={200} >
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box marginTopAuto padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex wrap', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex no wrap</H5>
      </Row>
      <Row bgIntent="highlight" flex flexNoWrap marginTop={3} width={200}>
        <Box marginBottomAuto padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
      <Row marginTop={3}>
        <H5>Flex with marginTopAuto in item 3</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} width={500} >
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ))
  .add('flex wrap', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex no wrap</H5>
      </Row>
      <Row bgIntent="highlight" flex flexNoWrap marginTop={3} width={200}>
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex Wrap </H5>
      </Row>
      <Row bgIntent="highlight" flex flexWrap marginTop={3} width={500} >
        {createExmapleBoxs(6)}
      </Row>
      <Row marginTop={3}>
        <H5>Flex Wrap Reverse</H5>
      </Row>
      <Row bgIntent="highlight" flex flexWrapReverse marginTop={3} width={500} >
        {createExmapleBoxs(6)}
      </Row>
    </Container>
  ))
  .add('flex with order', () => (
    <Container>
      <Row marginTop={3}>
        <H5>Flex Order</H5>
      </Row>
      <Row bgIntent="highlight" flex marginTop={3} >
        <Box padding={2} order={3} textIntent="dark" bgIntent="highlight">
          {'flex item 1'}
        </Box>
        <Box padding={2} order={2} textIntent="dark" bgIntent="highlight">
          {'flex item 2'}
        </Box>
        <Box padding={2} order={1} textIntent="dark" bgIntent="highlight">
          {'flex item 3'}
        </Box>
      </Row>
    </Container>
  ));

