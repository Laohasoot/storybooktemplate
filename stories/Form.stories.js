
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import Form from '../components/Form';
import FormGroup from '../components/FormGroup';
import Input from '../components/Input';
import Button from '../components/Button';
import Label from '../components/Label';
import { Container } from '../components';
import Row from '../components/Row';
import Col from '../components/Col';
import Select from '../components/Select';
import Option from '../components/Option';
import Radio from '../components/Radio';
import FormCheck from '../components/FormCheck';
import Checkbox from '../components/Checkbox';
import Card from '../components/Card';
import CardBody from '../components/CardBody';
import Textarea from '../components/Textarea';

storiesOf('Components|Form', module)
  .addDecorator(withKnobs)
  .add('simple form', () => (
    <Container className="mt-5">
      <Row>
        <Col md={6}>
          <Card>
            <CardBody>
              <Form>
                <FormGroup>
                  <Label htmlFor="fullName" >full name</Label>
                  <Input id="fullName" placeholder="John Wick" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="password">password</Label>
                  <Input id="password" type="password" placeholder="123456" />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="gender" >gender</Label>
                  <Select id="gender" >
                    <Option>male</Option>
                    <Option>female</Option>
                  </Select>
                </FormGroup>
                <FormGroup>
                  <FormCheck>
                    <Radio name="exampleRadio" value={text('value', 'test radio1')} id="exampleRadios1" disabled />
                    <Label type="checkbox" for="exampleRadios1" >exampleRadios1 disabled</Label>
                  </FormCheck>
                  <FormCheck>
                    <Radio name="exampleRadio" value={text('value', 'test radio2')} id="exampleRadios2" checked />
                    <Label type="checkbox" for="exampleRadios2" >exampleRadios2 checked</Label>
                  </FormCheck>
                  <FormCheck>
                    <Radio name="exampleRadio" value={text('value', 'test radio3')} id="exampleRadios3" />
                    <Label type="checkbox" for="exampleRadios3" >exampleRadios3</Label>
                  </FormCheck>
                </FormGroup>
                <FormGroup>
                  <FormCheck disabled>
                    <Checkbox name="exampleCheckbox" value={text('value', 'test checkbox1')} id="exampleCheckbox1" disabled />
                    <Label type="checkbox" for="exampleCheckbox1" >exampleCheckbox1</Label>
                  </FormCheck>
                  <FormCheck>
                    <Checkbox name="exampleCheckbox" value={text('value', 'test checkbox2')} id="exampleCheckbox2" checked />
                    <Label type="checkbox" for="exampleCheckbox2" >exampleCheckbox2 checked</Label>
                  </FormCheck>
                  <FormCheck>
                    <Checkbox name="exampleCheckbox" value={text('value', 'test checkbox3')} id="exampleCheckbox3" />
                    <Label type="checkbox" for="exampleCheckbox3" >exampleCheckbox3</Label>
                  </FormCheck>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="address" >address</Label>
                  <Textarea id="address" rows={5}>1234/4222 sukumbit street </Textarea>
                </FormGroup>
                <Button>Submit</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row >
    </Container >
  ))
  .add('Input Size', () => (
    <Container className="mt-5">
      <Row>
        <Col md={5}>
          <Card>
            <CardBody>
              <Form>
                <FormGroup>
                  <Input id="fullName" lg={boolean('lg', true)} placeholder="large size" />
                </FormGroup>
                <FormGroup>
                  <Input id="fullName" placeholder="nornal size" />
                </FormGroup>
                <FormGroup>
                  <Input id="fullName" sm={boolean('sm', true)} placeholder="small size" />
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row >
    </Container >
  ))
  .add('Select Size', () => (
    <Container className="mt-5">
      <Row>
        <Col md={5}>
          <Card>
            <CardBody>
              <Form>
                <FormGroup>
                  <Select id="fullName" lg={boolean('lg', true)} placeholder="large size" >
                    <Option>example1</Option>
                    <Option>example2</Option>
                  </Select>
                </FormGroup>
                <FormGroup>
                  <Select id="fullName" placeholder="nornal size" >
                    <Option>example1</Option>
                    <Option>example2</Option>
                  </Select>
                </FormGroup>
                <FormGroup>
                  <Select id="fullName" sm={boolean('sm', true)} placeholder="small size" >
                    <Option>example1</Option>
                    <Option>example2</Option>
                  </Select>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row >
    </Container >
  ))
  .add('inline form', () => (
    <Container className="mt-5">
      <Row>
        <Col md={5}>
          <Form inline>
            <FormGroup>
              <Label htmlFor="fullName" >full name</Label>
              <Input id="fullName" placeholder="John Wick" value={text('value', '')} />
            </FormGroup>
            <Button>Submit</Button>
          </Form>
        </Col>
      </Row >
    </Container >
  ))
  .add('formGroup row', () => (
    <Container className="mt-5">
      <Row>
        <Col md={6}>
          <Form>
            <FormGroup row>
              <Label htmlFor="email" sm={2}>Email</Label>
              <Col sm={10}>
                <Input id="email" placeholder="exmaple@demo.com" />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label htmlFor="password" sm={2}>password</Label>
              <Col sm={10}>
                <Input id="password" type="password" placeholder="123456" />
              </Col>
            </FormGroup>
          </Form>
        </Col>
      </Row >
    </Container >
  ));
