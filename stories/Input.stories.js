
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import Input from '../components/Input';

storiesOf('Components|Input', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Input />
  ));
