
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import InputGroup from '../components/InputGroup';
import { Container } from '../components';
import Row from '../components/Row';
import Col from '../components/Col';
import InputGroupPrepend from '../components/InputGroupPrepend';
import InputGroupAppend from '../components/InputGroupAppend';
import InputGroupText from '../components/InputGroupText';
import Input from '../components/Input';
import Label from '../components/Label';
import Textarea from '../components/Textarea';
import Button from '../components/Button';


storiesOf('Components|InputGroup', module)
  .addDecorator(withKnobs)
  .add('simple InputGroup', () => (
    <Container className="mt-5" >
      <Row>
        <Col md={6} >
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText>@</InputGroupText></InputGroupPrepend>
            <Input placeholder="username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
          </InputGroup>
          <InputGroup className="mb-3">
            <Input placeholder="Recipient's username" />
            <InputGroupAppend><InputGroupText>@example.com</InputGroupText></InputGroupAppend>
          </InputGroup>
          <Label htmlFor="basic-url">Your vanity URL</Label>
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText>https://example.com/users/</InputGroupText></InputGroupPrepend>
            <Input id="basic-url" />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText>With textarea</InputGroupText></InputGroupPrepend>
            <Textarea aria-label="With textarea" />
          </InputGroup>
        </Col>
      </Row>
    </Container>
  ))
  .add('Checkbox and Radio', () => (
    <Container className="mt-5" >
      <Row>
        <Col md={6} >
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText><input type="radio" /></InputGroupText></InputGroupPrepend>
            <Input placeholder="username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
          </InputGroup>
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText><input type="checkbox" /></InputGroupText></InputGroupPrepend>
            <Input placeholder="username" aria-label="Recipient's username" aria-describedby="basic-addon2" />
          </InputGroup>

        </Col>
      </Row>
    </Container>
  ))
  .add('multiple input', () => (
    <Container className="mt-5" >
      <Row>
        <Col md={6} >
          <InputGroup className="mb-3">
            <InputGroupPrepend><InputGroupText><input type="radio" /></InputGroupText></InputGroupPrepend>
            <Input placeholder="firstname" />
            <Input placeholder="lastname" />
          </InputGroup>
        </Col>
      </Row>
    </Container>
  ))
  .add('multiple addons', () => (
    <Container className="mt-5" >
      <Row>
        <Col md={6} >
          <InputGroup className="mb-3">
            <InputGroupPrepend>
              <Button color="outline-secondary">Button</Button>
            </InputGroupPrepend>
            <Input />
          </InputGroup>
          <InputGroup className="mb-3">
            <Input />
            <InputGroupAppend>
              <Button color="outline-secondary">Button</Button>
            </InputGroupAppend>
          </InputGroup>
        </Col>
      </Row>
    </Container>
  ));
