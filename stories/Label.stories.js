
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Label from '../components/Label';

storiesOf('Components|Label', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Label>
      {text('chrild field', 'Hello Label')}
    </Label>
  ));
