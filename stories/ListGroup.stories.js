
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ListGroup from '../components/ListGroup';

storiesOf('Components|ListGroup', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <ListGroup>
      {text('chrild field', 'Hello ListGroup')}
    </ListGroup>
  ));
