
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import Modal from '../components/Modal';
import { Container } from '../components';
import Row from '../components/Row';
import ModalDialog from '../components/ModalDialog';
import ModalContent from '../components/ModalContent';
import ModalHeader from '../components/ModalHeader';
import ModalTitle from '../components/ModalTitle';
import ModalBody from '../components/ModalBody';
import ModalFooter from '../components/ModalFooter';
import Button from '../components/Button';

storiesOf('Modal', module)
  .addDecorator(withKnobs)
  .add('simple Modal', () => (
    <Container>
      <Row>
        <Button data-toggle="modal" data-target="#exampleModal" >Open Modal</Button>
        <Modal id="exampleModal">
          <ModalDialog>
            <ModalContent >
              <ModalHeader>
                <ModalTitle> Modal Title </ModalTitle>
              </ModalHeader>
              <ModalBody> asodija </ModalBody>
              <ModalFooter>
                <Button intent="secondary" data-dismiss="modal"> Cancel</Button>
                <Button> Submit </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>

        </Modal>
      </Row>
    </Container>
  ))
  .add('center Modal', () => (
    <Container>
      <Row>
        <Button data-toggle="modal" data-target="#exampleModal" >Open Center Modal</Button>
        <Modal id="exampleModal">
          <ModalDialog centered>
            <ModalContent >
              <ModalHeader>
                <ModalTitle> Modal Title </ModalTitle>
              </ModalHeader>
              <ModalBody> asodija </ModalBody>
              <ModalFooter>
                <Button intent="secondary" data-dismiss="modal"> Cancel</Button>
                <Button> Submit </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>

        </Modal>
      </Row>
    </Container>
  ))
  .add('smal and large Modal', () => (
    <Container>
      <Row>
        <Button marginRight={3} data-toggle="modal" data-target="#smallModal" >Open Small Modal</Button>
        <Modal id="smallModal">
          <ModalDialog small>
            <ModalContent >
              <ModalHeader>
                <ModalTitle> Modal Title </ModalTitle>
              </ModalHeader>
              <ModalBody> asodija </ModalBody>
              <ModalFooter>
                <Button intent="secondary" data-dismiss="modal"> Cancel</Button>
                <Button> Submit </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>
        </Modal>
        <Button data-toggle="modal" data-target="#largeModal" >Open Large Modal</Button>
        <Modal id="largeModal">
          <ModalDialog large>
            <ModalContent >
              <ModalHeader>
                <ModalTitle> Modal Title </ModalTitle>
              </ModalHeader>
              <ModalBody> asodija </ModalBody>
              <ModalFooter>
                <Button intent="secondary" data-dismiss="modal"> Cancel</Button>
                <Button> Submit </Button>
              </ModalFooter>
            </ModalContent>
          </ModalDialog>
        </Modal>
      </Row>
    </Container>
  ));
