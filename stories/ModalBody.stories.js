
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalBody from '../components/ModalBody';

storiesOf('ModalBody', module)
  .addDecorator(withKnobs)
  .add('simple ModalBody', () => (
    <ModalBody>
       {text('chrild field','Hello ModalBody')}
    </ModalBody> 
   )
  )
