
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalContent from '../components/ModalContent';

storiesOf('ModalContent', module)
  .addDecorator(withKnobs)
  .add('simple ModalContent', () => (
    <ModalContent>
       {text('chrild field','Hello ModalContent')}
    </ModalContent> 
   )
  )
