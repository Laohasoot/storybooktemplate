
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalDialog from '../components/ModalDialog';

storiesOf('ModalDialog', module)
  .addDecorator(withKnobs)
  .add('simple ModalDialog', () => (
    <ModalDialog>
       {text('chrild field','Hello ModalDialog')}
    </ModalDialog> 
   )
  )
