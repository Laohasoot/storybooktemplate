
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalFooter from '../components/ModalFooter';

storiesOf('ModalFooter', module)
  .addDecorator(withKnobs)
  .add('simple ModalFooter', () => (
    <ModalFooter>
       {text('chrild field','Hello ModalFooter')}
    </ModalFooter> 
   )
  )
