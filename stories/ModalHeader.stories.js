
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalHeader from '../components/ModalHeader';

storiesOf('ModalHeader', module)
  .addDecorator(withKnobs)
  .add('simple ModalHeader', () => (
    <ModalHeader>
       {text('chrild field','Hello ModalHeader')}
    </ModalHeader> 
   )
  )
