
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import ModalTitle from '../components/ModalTitle';

storiesOf('ModalTitle', module)
  .addDecorator(withKnobs)
  .add('simple ModalTitle', () => (
    <ModalTitle>
       {text('chrild field','Hello ModalTitle')}
    </ModalTitle> 
   )
  )
