
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Navbar from '../components/Navbar';
import NavbarBrand from '../components/NavbarBrand';
import Collapse from '../components/Collapse';
import NavbarNav from '../components/NavbarNav/react/NavbarNav';
import NavItem from '../components/NavItem';
import NavLink from '../components/NavLink';
import NavbarToggler from '../components/NavbarToggler';
import NavbarTogglerIcon from '../components/NavbarTogglerIcon';
import Form from '../components/Form';
import { Container, Button } from '../components';
import Input from '../components/Input';

storiesOf('Components|Navbar', module)
  .addDecorator(withKnobs)
  .add('simple Navbar', () => (
    <Navbar expand={text('expand', 'lg')} className={text('className', 'bg-light')} theme={text('theme', 'light')}>
      <NavbarBrand >Navbar</NavbarBrand>
      <NavbarToggler
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      ><NavbarTogglerIcon />
      </NavbarToggler>
      <Collapse isNavbar id="navbarNav">
        <NavbarNav className="mr-auto">
          <NavItem>
            <NavLink href="#" active >Home</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Features</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Pricing</NavLink>
          </NavItem>
          <NavItem>
            <NavLink>Disabled</NavLink>
          </NavItem>
        </NavbarNav>
        <Form inline>
          <Input className="mr-sm-2" placeholder="Search" />
          <Button color="outline-primary">Search</Button>
        </Form>
      </Collapse>
    </Navbar>
  ))
  .add('navbar theme Navbar', () => (
    <Container className="mt-5">
      <Navbar expand="lg" className="bg-dark" theme="dark" >
        <NavbarBrand >Navbar</NavbarBrand>
        <NavbarToggler
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        ><NavbarTogglerIcon />
        </NavbarToggler>
        <Collapse isNavbar id="navbarNav">
          <NavbarNav className="mr-auto">
            <NavItem>
              <NavLink href="#" active >Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Features</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Pricing</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Disabled</NavLink>
            </NavItem>
          </NavbarNav>
          <Form inline>
            <Input className="mr-sm-2" placeholder="Search" />
            <Button color="outline-info">Search</Button>
          </Form>
        </Collapse>
      </Navbar>
      <Navbar expand="lg" className="bg-primary" theme="dark">
        <NavbarBrand >Navbar</NavbarBrand>
        <NavbarToggler
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        ><NavbarTogglerIcon />
        </NavbarToggler>
        <Collapse isNavbar id="navbarNav">
          <NavbarNav className="mr-auto">
            <NavItem>
              <NavLink href="#" active >Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Features</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Pricing</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Disabled</NavLink>
            </NavItem>
          </NavbarNav>
          <Form inline>
            <Input className="mr-sm-2" placeholder="Search" />
            <Button color="outline-light">Search</Button>
          </Form>
        </Collapse>
      </Navbar>
      <Navbar expand="lg" className="bg-light">
        <NavbarBrand >Navbar</NavbarBrand>
        <NavbarToggler
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        ><NavbarTogglerIcon />
        </NavbarToggler>
        <Collapse isNavbar id="navbarNav">
          <NavbarNav className="mr-auto">
            <NavItem>
              <NavLink href="#" active >Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Features</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Pricing</NavLink>
            </NavItem>
            <NavItem>
              <NavLink>Disabled</NavLink>
            </NavItem>
          </NavbarNav>
          <Form inline>
            <Input className="mr-sm-2" placeholder="Search" />
            <Button color="outline-primary">Search</Button>
          </Form>
        </Collapse>
      </Navbar>
    </Container>
  ));
