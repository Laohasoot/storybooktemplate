
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavbarBrand from '../components/NavbarBrand';

storiesOf('Components|NavbarBrand', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <NavbarBrand>
      {text('chrild field', 'Hello NavbarBrand')}
    </NavbarBrand>
  ));
