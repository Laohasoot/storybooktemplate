
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavbarNav from '../components/NavbarNav';

storiesOf('Components|NavbarNav', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <NavbarNav>
      {text('chrild field', 'Hello NavbarNav')}
    </NavbarNav>
  ));
