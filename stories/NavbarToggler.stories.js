
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavbarToggler from '../components/NavbarToggler';

storiesOf('Components|NavbarToggler', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <NavbarToggler>
      {text('chrild field', 'Hello NavbarToggler')}
    </NavbarToggler>
  ));
