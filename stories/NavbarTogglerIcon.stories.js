
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import NavbarTogglerIcon from '../components/NavbarTogglerIcon';

storiesOf('Components|NavbarTogglerIcon', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <NavbarTogglerIcon>
      {text('chrild field', 'Hello NavbarTogglerIcon')}
    </NavbarTogglerIcon>
  ));
