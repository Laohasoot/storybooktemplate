
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Radio from '../components/Radio';
import { Container } from '../components';
import Row from '../components/Row';
import Form from '../components/Form';
import Label from '../components/Label';
import FormCheck from '../components/FormCheck';

storiesOf('Components|Radio', module)
  .addDecorator(withKnobs)
  .add('basic radio', () => (
    <Container >
      <Row>
        <Form>
          <FormCheck>
            <Radio name="exampleRadio" value={text('value', 'test radio1')} id="exampleRadios1" />
            <Label type="checkbox" for="exampleRadios1" >exampleRadios1</Label>
          </FormCheck>
          <FormCheck>
            <Radio name="exampleRadio" value={text('value', 'test radio2')} id="exampleRadios2" />
            <Label type="checkbox" for="exampleRadios2" >exampleRadios2</Label>
          </FormCheck>
          <FormCheck>
            <Radio name="exampleRadio" value={text('value', 'test radio3')} id="exampleRadios3" />
            <Label type="checkbox" for="exampleRadios3" >exampleRadios3</Label>
          </FormCheck>
        </Form>
      </Row>
    </Container>
  ));
