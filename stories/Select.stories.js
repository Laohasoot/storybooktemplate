
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Select from '../components/Select';

storiesOf('Components|Select', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Select>
      {text('chrild field', 'Hello Select')}
    </Select>
  ));
