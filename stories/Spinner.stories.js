
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Spinner from '../components/Spinner';

storiesOf('Spinner', module)
  .addDecorator(withKnobs)
  .add('simple Spinner', () => (
    <Spinner>
       {text('chrild field','Hello Spinner')}
    </Spinner> 
   )
  )
