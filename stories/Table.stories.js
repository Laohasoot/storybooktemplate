
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Table from '../components/Table';
import TableHead from '../components/TableHead';
import Th from '../components/Th';
import Tr from '../components/Tr';
import Td from '../components/Td';
import TableBody from '../components/TableBody';
import { Container, Row, Col, Intent } from '../components';

storiesOf('Table', module)
  .addDecorator(withKnobs)
  .add('simple Table', () => (
    <Table >
      <TableHead dark>
        <Tr>
          <Th scope="col" >#</Th>
          <Th scope="col" >First</Th>
          <Th scope="col" >Last</Th>
          <Th scope="col" >Eamil</Th>
        </Tr>
      </TableHead >
      <TableBody>
        <Tr>
          <Td>1</Td>
          <Td>Isoon</Td>
          <Td>Laohasoot</Td>
          <Td>icepanda501@gmail.com</Td>
        </Tr>
        <Tr>
          <Td>2</Td>
          <Td>Jacob</Td>
          <Td>Thornton</Td>
          <Td>Jacob@gmail.com</Td>
        </Tr>
        <Tr>
          <Td>3</Td>
          <Td>Mark</Td>
          <Td>Otto</Td>
          <Td>Mark@gmail.com</Td>
        </Tr>
      </TableBody>
    </Table >
  ))
  .add('theme ligth and dark Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table dark>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
        <Col>
          <Table light>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('striped Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table striped dark>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
        <Col>
          <Table striped light>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('bordered Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table bordered dark>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
        <Col>
          <Table bordered light>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('borderless Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table borderless dark>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
        <Col>
          <Table borderless light>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('hoverable Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table hoverable dark>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
        <Col>
          <Table hoverable light>
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('Intent with Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table >
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr intent={Intent.active}>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.primary}>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.success}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.danger}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.warning}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.info}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.light}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr intent={Intent.dark}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr>
                <Td intent={Intent.primary}>3</Td>
                <Td intent={Intent.success}>Mark</Td>
                <Td intent={Intent.light}>Otto</Td>
                <Td intent={Intent.dark}>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ))
  .add('bgIntent with Table', () => (
    <Container marginTop={3}>
      <Row>
        <Col>
          <Table >
            <TableHead>
              <Tr>
                <Th scope="col" >#</Th>
                <Th scope="col" >First</Th>
                <Th scope="col" >Last</Th>
                <Th scope="col" >Eamil</Th>
              </Tr>
            </TableHead >
            <TableBody>
              <Tr bgIntent={Intent.active}>
                <Td>1</Td>
                <Td>Isoon</Td>
                <Td>Laohasoot</Td>
                <Td>icepanda501@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.primary}>
                <Td>2</Td>
                <Td>Jacob</Td>
                <Td>Thornton</Td>
                <Td>Jacob@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.success}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.danger}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.warning}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.info}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.light}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
              <Tr bgIntent={Intent.dark}>
                <Td>3</Td>
                <Td>Mark</Td>
                <Td>Otto</Td>
                <Td>Mark@gmail.com</Td>
              </Tr>
            </TableBody>
          </Table >
        </Col>
      </Row>
    </Container>
  ));

