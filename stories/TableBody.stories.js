
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import TableBody from '../components/TableBody';

storiesOf('TableBody', module)
  .addDecorator(withKnobs)
  .add('simple TableBody', () => (
    <TableBody>
       {text('chrild field','Hello TableBody')}
    </TableBody> 
   )
  )
