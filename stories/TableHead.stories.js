
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import TableHead from '../components/TableHead';

storiesOf('TableHead', module)
  .addDecorator(withKnobs)
  .add('simple TableHead', () => (
    <TableHead>
       {text('chrild field','Hello TableHead')}
    </TableHead> 
   )
  )
