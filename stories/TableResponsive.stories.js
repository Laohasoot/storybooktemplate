
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import TableResponsive from '../components/TableResponsive';

storiesOf('TableResponsive', module)
  .addDecorator(withKnobs)
  .add('simple TableResponsive', () => (
    <TableResponsive>
       {text('chrild field','Hello TableResponsive')}
    </TableResponsive> 
   )
  )
