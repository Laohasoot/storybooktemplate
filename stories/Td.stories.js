
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Td from '../components/Td';

storiesOf('Td', module)
  .addDecorator(withKnobs)
  .add('simple Td', () => (
    <Td>
       {text('chrild field','Hello Td')}
    </Td> 
   )
  )
