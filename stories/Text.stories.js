
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean } from '@storybook/addon-knobs';
import Text from '../components/Text';

storiesOf('Components|Text', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Text
      intent={text('intent', 'primary')}
      fontWeight={text('fontWeight', 'bold')}
      italic={boolean('italic', false)}
    >
      {text('chrild field', 'Hello Text')}
    </Text>
  ));
