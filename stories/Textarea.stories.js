
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Textarea from '../components/Textarea';

storiesOf('Components|Textarea', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Textarea>
      {text('chrild field', 'Hello Textarea')}
    </Textarea>
  ));
