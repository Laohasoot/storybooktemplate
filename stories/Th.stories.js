
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Th from '../components/Th';

storiesOf('Th', module)
  .addDecorator(withKnobs)
  .add('simple Th', () => (
    <Th>
       {text('chrild field','Hello Th')}
    </Th> 
   )
  )
