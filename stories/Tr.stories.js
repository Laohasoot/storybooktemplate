
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import Tr from '../components/Tr';

storiesOf('Tr', module)
  .addDecorator(withKnobs)
  .add('simple Tr', () => (
    <Tr>
       {text('chrild field','Hello Tr')}
    </Tr> 
   )
  )
