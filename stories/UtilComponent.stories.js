
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import UtilComponent from '../components/UtilComponent';

storiesOf('Utility|UtilComponent', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <UtilComponent>
      {text('chrild field', 'Hello UtilComponent')}
    </UtilComponent>
  ));
