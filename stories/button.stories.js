import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { withKnobs, text } from '@storybook/addon-knobs';

import { Button, Container } from '../components';

storiesOf('Components|Button', module)
  .addDecorator(withKnobs)
  .add('simple component', () => (
    <Container>
      <Button onClick={action('clicked')} >
        {text('chrild field', 'Hello Button')}
      </Button>
    </Container>
  ));

