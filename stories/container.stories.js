import React from 'react';

import { storiesOf } from '@storybook/react';
import { withKnobs, number } from '@storybook/addon-knobs';
import { Container } from '../components';
import '../components/Container/Container.css';
import Row from '../components/Row';
import Col from '../components/Col';


storiesOf('Layout|Grid', module)
  .addDecorator(withKnobs)
  .add('container and row', () => (
    <Container className="mt-3">
      <Row className="mb-3">
        <Col className="example">
          md = 0
        </Col>
        <Col className="example">
          md = 0
        </Col>
        <Col className="example">
          md = 0
        </Col>
      </Row>
      <Row>
        <Col className="example" md={number('exmaple1 md', 0)}>
          example 1
        </Col>
        <Col className="example" md={number('example2 md', 6)}>
          example 2
        </Col>
        <Col className="example" md={number('example3 md', 0)}>
          example 3
        </Col>
      </Row>
    </Container>
  ));

